#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <array>

class base_transaction {
public:
	base_transaction(std::vector<char>& data_stream);
	virtual ~base_transaction();
	
	base_transaction(base_transaction const&) = delete;
	base_transaction& operator=(base_transaction const&) = delete;

	virtual void commit();
	std::size_t size() const;

	void add_int(int const);
	void add_float(float const);
	void add_str(std::string const&);
private:
	std::vector<char> data;
	std::vector<char>& data_stream;
};

class subrecord_transaction : public base_transaction {
public:
	subrecord_transaction(std::vector<char>&);
	~subrecord_transaction(); // No virtual destructor, effectively final, can have no subclasses

	void commit() override;
private:
	static int const word_byte_size;
	std::vector<char>& data_stream;
};


