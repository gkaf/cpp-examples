#include <cstdlib>
#include <vector>
#include <iostream>
#include <string>

using int_unit_op = int(int);

template<int_unit_op f>
void map_to_int_vector(std::vector<int>& v) {
	for ( std::vector<int>::iterator it = v.begin(); it < v.end(); ++it ) {
		*it = f(*it);
	}
}

template<typename T>
using unit_op = T(T);

template<typename T, unit_op<T> f>
void map_to_vector(std::vector<T>& v) {
	for ( typename std::vector<T>::iterator it = v.begin(); it < v.end(); ++it ) {
		*it = f(*it);
	}
}

template<typename T>
void print_vector(std::vector<T>& v) {
	for ( T const& x : v ) {
		std::cout << std::to_string(x) << ", ";
	}
	std::cout << "\n";
}

int quadruple(int n)
{
	return 4*n;
}

int main() {
	std::vector<int> v{1,2,3};
	map_to_int_vector<quadruple>(v);
	print_vector(v);

	std::vector<int> w{1,2,3};
	map_to_vector<int,quadruple>(w);
	print_vector(w);

	return EXIT_SUCCESS;
}
