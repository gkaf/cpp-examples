#include "inheritance.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <array>

int const subrecord_transaction::word_byte_size = 4;

void base_transaction::add_int(int const n)
{
	char const* const c = reinterpret_cast<char const*>(&n);
	data.insert(std::end(data), c, c + sizeof(int));
}

void base_transaction::add_float(float const x)
{
	char const* const c = reinterpret_cast<char const*>(&x);
	data.insert(std::end(data), c, c + sizeof(float));
}

void base_transaction::add_str(std::string const& s)
{
	char const* const c = s.c_str();
	data.insert(std::end(data), c, c + s.size());
}

std::size_t base_transaction::size() const
{
	return data.size();
}

base_transaction::base_transaction(std::vector<char>& data_stream) :
	data(), data_stream(data_stream)
{}

base_transaction::~base_transaction()
{
	// Commit it or lose it
}

void base_transaction::commit()
{
	if ( size() > 0 ) { 
		int const s = static_cast<int>(size());	
		char const* const c = reinterpret_cast<char const*>(&s);

		data_stream.insert(std::end(data_stream), c, c + sizeof(int));
		data_stream.insert(std::end(data_stream), std::begin(data), std::end(data));
		data_stream.insert(std::end(data_stream), c, c + sizeof(int));

		data.clear();
	}
}

subrecord_transaction::subrecord_transaction(std::vector<char>& data_stream) :
	base_transaction(data_stream), data_stream(data_stream)
{}

subrecord_transaction::~subrecord_transaction() {}

void subrecord_transaction::commit()
{
	if (size() > 0) {
		base_transaction size_bare_record(data_stream);

		int const size_in_words = size()/word_byte_size;
	
		size_bare_record.add_int(size_in_words);
		size_bare_record.commit();
	}

	base_transaction::commit();
}

static void print_stream(std::string const& description, std::vector<char> const& s)
{
	std::cout << description << "\n";

	std::array<std::array<char,sizeof(char)*8>,4> word{};
	for (std::array<char,8> byte : word) {
		for (char& c : byte) {
			c = '0';
		}
	}

	std::size_t n = 0;
	for (char const c : s) {
		for (std::size_t i = 0; i < sizeof(char)*8; ++i) {
			std::array<char,8>& byte = word[n];
			char& cp = byte[i];
			// c << i == c*(2^i) mod (2^32)
			// z = x & y -> z[i] = 1 if x[i] = 1 and y[i] = 1, 0 otherwise
			bool const is_unit = ( static_cast<char>(0b1 << i) & c ) != 0;
			if (is_unit) {
				cp = '1';
			} else {
				cp = '0';
			}
		}
		if (n >= 3) {
			for (std::size_t i = 0; i < 4; ++i) {
				std::cout << std::string(std::begin(word[i]), std::end(word[i]));
				if ( i >= 3 ) {
					std::cout << "\n";
				} else {
					std::cout << " ";
				}
			}
			for (std::array<char,8> byte : word) {
				for (char& cp : byte) {
					cp = '0';
				}
			}
			n = 0;
		} else {
			n += 1;
		}
	}
	
	std::cout << "\n";
}

int main()
{
	std::vector<char> data_stream_br;
	std::vector<char> data_stream_tr;
	
	{
		base_transaction br(data_stream_br);
		subrecord_transaction tr(data_stream_tr);

		br.add_int(1);
		br.add_int(2);
		br.add_int(3);
		br.commit();

		tr.add_int(1);
		tr.add_int(2);
		tr.add_int(3);
		tr.commit();
	}
	
	print_stream("Base record: ", data_stream_br);
	print_stream("Typed record: ", data_stream_tr);

	base_transaction br(data_stream_br);
	// std::string has a non-explicit constructor with a 'char const*' as its single input:
	char const* const message = "Smth.";
	br.add_str(message); // calling base_transaction::add_str(std::string const&) with char const* input!

	return 0;

	// messsage not commited thus lost
}
