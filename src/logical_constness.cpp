#include <cstdlib>
#include <vector>
#include <iostream>
#include <string>

template <typename T>
class Container {
	public:
		Container(std::size_t const length);
		~Container();
		
		void set(std::size_t const i, T const x);
		T& get(std::size_t const i);
	private:
		std::size_t length;
		T* data;
};

template <typename T>
class Container<T const> {
	public:
		Container(std::size_t const length);
		~Container();
		
		T const& get(std::size_t const i);
	private:
		std::size_t const length;
		T const* const data;
};

template<typename T>
Container<T>::Container(std::size_t const length) :
	length(length), data(new T[length])
{}

template<typename T>
Container<T>::~Container() {
	delete[] data;
}

template<typename T>
void Container<T>::set(std::size_t const i, T const x) {
	data[i] = x;
}

template<typename T>
T& Container<T>::get(std::size_t const i) {
	return data[i];
}

template<typename T>
Container<const T>::Container(std::size_t const length) :
	length(length), data(new T[length])
{}

template<typename T>
Container<const T>::~Container() {
	delete[] data;
}

template<typename T>
T const& Container<const T>::get(std::size_t const i) {
	return data[i];
}

int main() {
	Container<const int> c0(12);
	//c0.get(10) = 2;

	Container<int> c1(12);
	c1.get(10) = 2;

	return EXIT_SUCCESS;
}
