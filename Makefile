#include make.inc

define set_config_build_dir
$(if $(or $(filter $(2), build), $(filter $(2), debug)),
	$(eval $(1):=$(2)),
	$(error Unsuported target: $(2))
)
endef

define set_config_var
$(if $(filter $(1), build),$(eval $(2):=$(3)))
$(if $(filter $(1), debug),$(eval $(2):=$(4)))
endef

ifndef CONFIG
CONFIG:=build
endif

## Build directory setup

$(call set_config_build_dir,BUILD_DIR,$(CONFIG))

OBJ_DIR := $(BUILD_DIR)/obj
BIN_DIR := $(BUILD_DIR)/bin
LIB_DIR := $(BUILD_DIR)/lib

HEADER_DIR := include

SRC_DIR := src

## Compiler setup

CC := g++
CFLAGS_BUILD := -std=c++11 -W -Wall 
CFLAGS_DEBUG := -std=c++11 -W -Wall -g
$(call set_config_var,$(CONFIG),CFLAGS,$(CFLAGS_BUILD),$(CFLAGS_DEBUG))

CFORTRAN := gfortran
FFLAGS_BUILD := -O3
FFLAGS_DEBUG := -g
$(call set_config_var,$(CONFIG),FFLAGS,$(FFLAGS_BUILD),$(FFLAGS_DEBUG))

LINKER := $(CC)
LFLAGS_BUILD := -Wl,-rpath=
LFLAGS_DEBUG := $(LFLAGS_BUILD)
$(call set_config_var,$(CONFIG),LFLAGS,$(LFLAGS_BUILD),$(LFLAGS_DEBUG))

# The archiver and the flag(s) to use when building archive (library)
ARCH := ar
ARCHFLAGS := cr
RANLIB := echo
RANLIB_PATH := $(shell which ranlib)
ifdef RANLIB_PATH
	RANLIB := ranlib
endif

## Library inclusions

INCLUDE_PATH_LIST := $(HEADER_DIR)

INCLUDE_PATHS := $(patsubst %, -I%, $(INCLUDE_PATH_LIST))

## Library linking

LIB_PATHS :=
LIB_LINKS := 
LIB_LINKS_FLAGS :=

## Setup for the tutorial code

HEADER_LIST := 

HEADERS := $(patsubst %, $(HEADER_DIR)/%, $(HEADER_LIST))

SOURCE_LIST := inheritance.cpp template_with_variable.cpp logical_constness.cpp

SOURCES := $(patsubst %, $(SRC_DIR)/%, $(SOURCE_LIST))
OBJECTS := $(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SOURCES))
BINARIES := $(patsubst $(OBJ_DIR)/%.o, $(BIN_DIR)/%, $(OBJECTS))

# Targets

.PHONY: all
all: prepare tutorial

.PHONY: tutorial
tutorial: $(BINARIES)

## Compilation
$(OBJECTS): $(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(HEADERS) | $(OBJ_DIR)
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE_PATHS)

$(BINARIES): $(BIN_DIR)/%: $(OBJ_DIR)/%.o | $(BIN_DIR)
	$(LINKER) -o $@ $^ $(LFLAGS) $(LIB_PATHS) $(LIB_LINKS_FLAGS) $(LIB_LINKS)

## Housekeeping

.PHONY: prepare
prepare: $(OBJ_DIR) $(BIN_DIR) $(LIB_DIR)
	@{ \
		if [ -z $(RANLIB_PATH) ]; then \
			echo "Warning, no ranlib installed, using: ranlib = $(RANLIB)"; \
		fi; \
	}

$(OBJ_DIR) $(BIN_DIR) $(LIB_DIR) : % : | $(BUILD_DIR)
	mkdir $@

$(BUILD_DIR):
	mkdir $@

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)
